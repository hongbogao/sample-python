#This code uses jason talking to google maps API. Pull data out of the internet and print the place ID from the jason file.
#In the end you can find place ID and types as well in the jason well. Here we print ID as an example.
import urllib.request, urllib.parse, urllib.error
import json
import ssl
api_key = False
if api_key is False:
    api_key = 42
    serviceurl = 'http://py4e-data.dr-chuck.net/json?'
else :
    serviceurl = 'https://maps.googleapis.com/maps/api/geocode/json?'
# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE
address = input('Enter location: ')
parms = dict()
parms['address'] = address
if api_key is not False: parms['key'] = api_key
url = serviceurl + urllib.parse.urlencode(parms)
print('Retrieving', url)
uh = urllib.request.urlopen(url, context=ctx)
#open a page, turn the utf-8 code into unicode.
data = uh.read().decode()
#check how many characters we got.
print('Retrieved', len(data), 'characters')
try:
    js = json.loads(data)
except:
    js = None
#Debugging : quit if following conditions are not fullfilled.
if not js or 'status' not in js or js['status'] != 'OK':
    print('==== Failure To Retrieve ====')
    print(data)
place_id = js['results'][0]['place_id']
print("Place id ", place_id)
